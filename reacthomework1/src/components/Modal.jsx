import React, { Component } from "react";
import PropTypes from 'prop-types';

export class Modal extends Component {
  render() {
    return (
        <div className="modal-cover">
            <div  className="modal-body" onClick={(e) => {e.stopPropagation();}}>
                <div style={{backgroundColor: this.props.backgroundColor}} className="head-container">
                    <header className="header" >{this.props.header}
                    </header>
                {this.props.closeIcon && <button className = "close-modal-btn" onClick = {this.props.toggle} className="close-modal-btn">x</button>}
                </div>
                    <p className="main-text">{this.props.text}</p>
                {this.props.action}

            </div>
        </div>
    )
  }
}
Modal.propTypes = {
  backgroundColor: PropTypes.string,
  header: PropTypes.string,
  closeIcon: PropTypes.bool,
  toggle: PropTypes.func,
  text: PropTypes.string,
  action: PropTypes.array
}
